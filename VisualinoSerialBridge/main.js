
// Port used by the Websockets server
var SERVER_PORT = 13478;
//var SERVER_PORT = 80;


var avaliable_ports_list;
// Callback function for when we get a new list of available COM ports on the system
function onGetPortsList(ports) 
{
	// Update a list with the available port names
	avaliable_ports_list = [];
	for (var i=0; i<ports.length; i++) 
	{
		avaliable_ports_list.push(ports[i].path);
	}

	if (avaliable_ports_list.indexOf(disconnected_port) !== -1)
		clearTimeout(check_available_ports_timeout);


	// Update the List of COM ports available in the UI
	var html_ports = "<table><tr><th>Available Ports</th></tr>"
	for (var i=0; i<ports.length; i++) 
	{
		console.log(ports[i].path);
		html_ports += "<tr><td>"+ports[i].path+"</td></tr>";
	}
	html_ports += "</table>";
	$('#div_ports').html(html_ports);


	// If any websocket client is connected, send him the port list

	var ports_list = "PORTS," + connectedPortName;
	// Iterate the list, constructins a string with all the ports
	for (var i=0; i<ports.length; i++)
	{
		ports_list += ','+ports[i].path;
	};
	// Send the string with all the port names
	broadcastWebsocketMessage(ports_list);

	//console.log($('#div_ports'));
}


function getPortsList()
{
	chrome.serial.getDevices(onGetPortsList);
}

getPortsList();


chrome.serial.onReceive.addListener(onReceiveCallback);

chrome.serial.onReceiveError.addListener(onReceiveErrorCallback);


// ID and ame of the connected COM port
var connectedPortID = null;
var connectedPortName = "";

 
function onPortConnect( connectionInfo ) 
{
	console.log("onPortConnect");
	console.log(connectionInfo);

	if (!connectionInfo) return;

	// The serial port has been opened. Save its id to use later.
	connectedPortID = connectionInfo.connectionId;

	// Do whatever you need to do with the opened port.

	broadcastWebsocketMessage('CONNECT,'+connectedPortName);

	log("Connected to COM port!");
}


var check_available_ports_timeout = null;
var disconnected_port = "";


function onPortDisconnect() 
{
	console.log("onPortDisconnect");

	//console.log(connectionInfo);
	// The serial port has been opened. Save its id to use later.
	connectedPortID = null;

	// Do whatever you need to do with the opened port.

	broadcastWebsocketMessage('CONNECT,'+connectedPortName);

	log("Disconnected from COM port!");
}



// Connect to the a specific COM port (by specified port name)
function connectToPort(name)
{
	log("Trying to connect to port:"+name);
	if (avaliable_ports_list.indexOf(name) === -1)
	{
		log("Invalid port name!");
		return;
	}

	clearTimeout(check_available_ports_timeout);


	connectedPortName = name;
	log("Valid port");
	chrome.serial.connect(name, {bitrate: 115200}, onPortConnect);

}


// Disconnect from the a specific COM port (by specified port name)
function disconnectFromPort()
{
	connectedPortName = "";
	if (connectedPortID !== null)
		chrome.serial.disconnect(connectedPortID, onPortDisconnect);
}

// Convert a buffer array to a String
function convertArrayBufferToString (buf) 
{
	return String.fromCharCode.apply(null, new Uint8Array(buf));
}


// Add a line of data to the log window on the UI
function log(text) 
{
	$('#textarea_log').append(text + '\n');

	var psconsole = $('#textarea_log');

    if(psconsole.length)
       psconsole.scrollTop(psconsole[0].scrollHeight - psconsole.height());
}


var stringReceived = '';

function onReceiveCallback(info) 
{
	//console.log("onReceiveCallback");
	//console.log(info.data);
	if (info.connectionId == connectedPortID && info.data) 
	{
		var str = convertArrayBufferToString(info.data);

		var tokens =  str.split('\n');
		// No terminator received, just add the data to the buffer
		if (tokens.length == 1)
		{
			stringReceived += str;
		}
		// If at least a terminator was received, send the data
		else
		{
			//console.log("buffer:"+stringReceived);
			var msg = stringReceived+tokens[0];
			broadcastWebsocketMessage(msg);
			//console.log('<-  ' +msg);

			for (var i=1; i<tokens.length-1; i++)
			{
				var msg = tokens[i];
				broadcastWebsocketMessage(msg);
				console.log('<-  ' +msg);
			}

			stringReceived = tokens[tokens.length-1];
			//console.log('##remains:'+(stringReceived));
		}
	}
};


// Checks the list of 
function checkAvailablePorts(port)
{
	console.log("checkAvailablePorts:"+port);
	clearTimeout(check_available_ports_timeout);
	if (port) disconnected_port = port;
	check_available_ports_timeout = setTimeout(checkAvailablePorts, 2000);
	getPortsList();
}


function onReceiveErrorCallback(info) 
{
	console.log("onReceiveErrorCallback");
	console.log(info);
	console.log(connectedPortID);

	if (info.connectionId == connectedPortID)
	{
		console.log("ERRO ERRO ERRO OMG!!!");

		/*
		checkAvailablePorts(connectedPortName);

		connectedPortID = null;
		connectedPortName = "";
		*/
	
		disconnectFromPort();
	}
};


// Sends a message to all connected websocket clients
function broadcastWebsocketMessage(data)
{
	// Echo the message to all connected websockets
	for (var i = 0; i < connectedSockets.length; i++) connectedSockets[i].send(data);
}


// Process the incoming messages from the UI
function processWebsocketMessage(e)
{
	//log(e.data);

	// Echo the message to all connected websockets
	//broadcastWebsocketMessage(e.data);


	// Let's see if we need to process the message, or just forward it to the serial port
	var tokens = e.data.split(',');

	// If the message starts with a #, it's a server command, so let's process it here
	if (tokens[0][0] === '#')
	{
		switch(tokens[0])
		{
			// CONNECT - Connect message
			// This message requests the server to connect to a specific serial port
			case '#CONNECT':
				var port = tokens[1];
				log("Opening port:#"+port+'#');

				connectToPort(port);

/*

				// Set the callback for when the connection is established
				serialPort.on("close", function () 
				{
					open_port = '';
					console.log('close_port:'+open_port);
					if (websocket_client) websocket_client.send('CONNECT,'+open_port);
				});

				// Not that everything is configured, we try to establish the connection
				serialPort.open(function (error) 
				{
					if ( error ) 
					{
						console.log('Error: '+error);
					} 
				});
*/

			break;

			// DISCONNECT - Disconnect message
			// This message contains requests the server to disconnect from the currently connected port
			case '#DISCONNECT':
				log("#DISCONNECT command received...");
				disconnectFromPort();
				//if (serialPort) serialPort.close();
			break;

		}
	}
	// If the command is NOT a server command, forward it to the arduino
	else
	{
		// Make sure we are connested to a port before sending data
		if (connectedPortID !== null) 
		{
			sendSerial(e.data);

			//fs.appendFile("log.txt", ' -> ' + data, function(err) {if(err) {return console.log(err); } });
		}
	}

};


function onWrite()
{
	//console.log("onWrite");
}



// Convert string to ArrayBuffer
function convertStringToArrayBuffer(str) 
{
	var buf=new ArrayBuffer(str.length);
	var bufView=new Uint8Array(buf);
	for (var i=0; i<str.length; i++) 
	{
		bufView[i]=str.charCodeAt(i);
	}
	return buf;
}

function sendSerial (data) 
{
	//console.log("sendSerial");
	console.log(' -> ' + data);
	var buffer = new Uint8Array(data);
	//console.log(connectedPortID);
	//console.log(data);
	//console.log(buffer);

	chrome.serial.send(connectedPortID, convertStringToArrayBuffer(data), onWrite);
}


var isServer = false;
if (http.Server && http.WebSocketServer) 
{
	// Listen for HTTP connections.
	var server = new http.Server();
	var wsServer = new http.WebSocketServer(server);
	server.listen(SERVER_PORT);
	isServer = true;

	
	server.addEventListener('request', function(req) {
	var url = req.headers.url;
	if (url == '/')
	  url = '/index.html';
	// Serve the pages of this chrome application.
	req.serveUrl(url);
	return true;
	});
	

	// A list of connected websockets.
	var connectedSockets = [];

	wsServer.addEventListener('request', function(req) 
	{
		log('Client connected');
		var socket = req.accept();
		connectedSockets.push(socket);

		// Add the callback to process the incoming websocket messages
		socket.addEventListener('message', processWebsocketMessage );


		getPortsList();

		
		// When a socket is closed, remove it from the list of connected sockets.
		socket.addEventListener('close', function() 
		{
			log('Client disconnected');
			for (var i = 0; i < connectedSockets.length; i++) 
			{
				if (connectedSockets[i] == socket) 
				{
					connectedSockets.splice(i, 1);
					break;
				}
			}
		});
		

		return true;
	});
}

/*
chrome.runtime.onClose.addListener(function() 
{
	alert("Stopping...");
    server.stop();
});
*/


document.addEventListener('DOMContentLoaded', function() 
{
	log("Visualino Serial Bridge started.");
	log("Listening on port "+SERVER_PORT);
	// FIXME: Wait for 1s so that HTTP Server socket is listening...
	/*
	setTimeout(function() {
		var address = isServer ? 'ws://localhost:' + SERVER_PORT + '/' :
		  window.location.href.replace('http', 'ws');
		var ws = new WebSocket(address);
		ws.addEventListener('open', function() {
		log('Connected');
		});

		ws.addEventListener('close', function() {
		log('Connection lost');
		//$('input').disabled = true;
		});
		ws.addEventListener('message', function(e) {
		log(e.data);
		});
	}, 1e3);
	*/

});




